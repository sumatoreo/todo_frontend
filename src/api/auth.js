import axios from '@/api/axios'

class AuthService {
  signUp(credentials) {
    return axios.post('profile/sign-up', {
      name: credentials.name,
      email: credentials.email,
      password: credentials.password,
      password_confirmation: credentials.password_confirmation,
    })
  }

  signIn(credentials) {
    return axios.post('profile/sign-in', {
      email: credentials.email,
      password: credentials.password,
    })
  }

  signOut() {
    return axios.post('profile/sign-out').then(({data}) => data)
  }

  getProfile() {
    return axios.get('profile')
  }

  googleCallback(credentials) {
    return axios
      .get('callback/google', {params: credentials})
      .then(({data}) => data)
  }

  googleAuth() {
    return axios.get('redirect/google').then(({data}) => data)
  }
}

export default new AuthService()
