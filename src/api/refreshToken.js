import axios from 'axios'

export const refreshAccessToken = refreshToken => {
  return axios
    .post('profile/refresh-token', {
      refresh_token: refreshToken
    })
    .then(({data}) => data)
}
