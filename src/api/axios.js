import axios from 'axios'
import {getItem, removeItem, setItem} from '@/helpers/persistanceStorage'
import {refreshAccessToken} from '@/api/refreshToken'

axios.defaults.baseURL = 'https://api.todo.app/api/v1'

axios.interceptors.request.use(
  async (config) => {
    const token = getItem('accessToken')

    config.headers.Authorization = token ? `Bearer ${token}` : ''
    config.headers.Accept = 'application/json'

    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

/**
 * Refresh Token
 */
const createAxiosResponseInterceptor = () => {
  const interceptor = axios.interceptors.response.use(
    (response) => response,
    async (error) => {
      const refreshToken = getItem('refreshToken')

      if (error.response.status !== 401 || !refreshToken) {
        return Promise.reject(error)
      }

      axios.interceptors.response.eject(interceptor)

      const response = await refreshAccessToken(refreshToken)
        .then((tokens) => {
          setItem('accessToken', tokens.access_token)
          setItem('refreshToken', tokens.refresh_token)
          error.response.config.headers[
            'Authorization'
          ] = `Bearer ${tokens.access_token}`

          return Promise.resolve(error.response.config)
        })
        .catch((error) => {
          removeItem('accessToken')
          removeItem('refreshToken')

          window.location.href = '/'

          return Promise.reject(error)
        })
        .finally(createAxiosResponseInterceptor)

      return axios(response)
    }
  )
}

createAxiosResponseInterceptor()

export default axios
