import axios from '@/api/axios'

class CatalogDataService {
  getAll(params) {
    return axios.get('catalogs', {params})
  }
  create(credentials) {
    return axios.post('catalogs', {
      name: credentials.name,
    })
  }
  update(credentials) {
    return axios.put('catalogs/' + credentials.id, {
      name: credentials.name,
    })
  }
  delete(credentials) {
    return axios.delete('catalogs/' + credentials.id)
  }
  composition(credentials, params) {
    return axios.get('catalogs/' + credentials.id + '/composition', {params})
  }
  addComposition(credentials) {
    return axios.post('catalogs/composition', credentials)
  }
  updateComposition(credentials) {
    return axios.put('catalogs/composition/' + credentials.id, credentials)
  }
  deleteComposition(credentials) {
    return axios.delete('catalogs/composition/' + credentials.id , )
  }
}

export default new CatalogDataService()
