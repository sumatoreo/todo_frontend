import CatalogDataService from '@/api/catalog'

const state = {
  isLoading: false,
  catalogs: null,
  data: null,
}

export const mutationTypes = {
  // getCatalogs
  getCatalogsStart: '[catalog] getCatalogsStart',
  getCatalogsSuccess: '[catalog] getCatalogsSuccess',
  getCatalogsFailed: '[catalog] getCatalogsFailed',

  // getCatalog by ID
  getCatalogStart: '[catalog] getCatalogStart',
  getCatalogSuccess: '[catalog] getCatalogSuccess',
  getCatalogFailed: '[catalog] getCatalogFailed',

  // createCatalog
  createCatalogStart: '[catalog] createCatalogStart',
  createCatalogSuccess: '[catalog] createCatalogSuccess',
  createCatalogFailed: '[catalog] createCatalogFailed',

  // updateCatalog
  updateCatalogStart: '[catalog] updateCatalogStart',
  updateCatalogSuccess: '[catalog] updateCatalogSuccess',
  updateCatalogFailed: '[catalog] updateCatalogFailed',

  // deleteCatalog
  deleteCatalogStart: '[catalog] deleteCatalogStart',
  deleteCatalogSuccess: '[catalog] deleteCatalogSuccess',
  deleteCatalogFailed: '[catalog] deleteCatalogFailed',

  // getComposition
  getCompositionStart: '[catalog] getCompositionStart',
  getCompositionSuccess: '[catalog] getCompositionSuccess',
  getCompositionFailed: '[catalog] getCompositionFailed',

  // getComposition
  addCompositionStart: '[catalog] addCompositionStart',
  addCompositionSuccess: '[catalog] addCompositionSuccess',
  addCompositionFailed: '[catalog] addCompositionFailed',

  // updateComposition
  updateCompositionStart: '[catalog] updateCompositionStart',
  updateCompositionSuccess: '[catalog] updateCompositionSuccess',
  updateCompositionFailed: '[catalog] updateCompositionFailed',

  // deleteComposition
  deleteCompositionStart: '[catalog] deleteCompositionStart',
  deleteCompositionSuccess: '[catalog] deleteCompositionSuccess',
  deleteCompositionFailed: '[catalog] deleteCompositionFailed',
}

export const actionTypes = {
  getCatalogs: '[catalog] getCatalogs',
  deleteCatalog: '[catalog] deleteCatalog',
  updateCatalog: '[catalog] updateCatalog',
  createCatalog: '[catalog] createCatalog',
  getComposition: '[catalog] getComposition',
  addComposition: '[catalog] addComposition',
  updateComposition: '[catalog] updateComposition',
  deleteComposition: '[catalog] deleteComposition',
}

export const getterTypes = {
  list: '[catalog] list',
}

const getters = {
  [getterTypes.list]: (state) => {
    return state.list
  },
}

const mutations = {
  // getCatalogs
  [mutationTypes.getCatalogsStart](state) {
    state.isLoading = true
  },
  [mutationTypes.getCatalogsSuccess](state, payload) {
    state.isLoading = false
    state.data = payload
  },
  [mutationTypes.getCatalogsFailed](state) {
    state.isLoading = false
    state.data = null
  },

  // deleteCatalog
  [mutationTypes.deleteCatalogStart](state) {
    state.isLoading = true
  },
  [mutationTypes.deleteCatalogSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.deleteCatalogFailed](state) {
    state.isLoading = false
  },

  // updateCatalog
  [mutationTypes.updateCatalogStart](state) {
    state.isLoading = true
  },
  [mutationTypes.updateCatalogSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.updateCatalogFailed](state) {
    state.isLoggedIn = false
  },

  // createCatalog
  [mutationTypes.createCatalogStart](state) {
    state.isLoading = true
  },
  [mutationTypes.createCatalogSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.createCatalogFailed](state) {
    state.isLoggedIn = false
  },

  // getComposition
  [mutationTypes.getCompositionStart](state) {
    state.isLoading = true
  },
  [mutationTypes.getCompositionSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.getCompositionFailed](state) {
    state.isLoggedIn = false
  },

  // addComposition
  [mutationTypes.addCompositionStart](state) {
    state.isLoading = true
  },
  [mutationTypes.addCompositionSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.addCompositionFailed](state) {
    state.isLoggedIn = false
  },

  // updateComposition
  [mutationTypes.updateCompositionStart](state) {
    state.isLoading = true
  },
  [mutationTypes.updateCompositionSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.updateCompositionFailed](state) {
    state.isLoggedIn = false
  },

  // deleteComposition
  [mutationTypes.deleteCompositionStart](state) {
    state.isLoading = true
  },
  [mutationTypes.deleteCompositionSuccess](state) {
    state.isLoading = false
  },
  [mutationTypes.deleteCompositionFailed](state) {
    state.isLoggedIn = false
  },
}

const actions = {
  [actionTypes.getCatalogs](context, credentials) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.getCatalogsStart)

      CatalogDataService.getAll(credentials)
        .then((response) => {
          context.commit(mutationTypes.getCatalogsSuccess, response.data)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.getCatalogsFailed)
        })
    })
  },

  [actionTypes.deleteCatalog](context, credentials) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.deleteCatalogStart)

      CatalogDataService.delete(credentials)
        .then((response) => {
          context.commit(mutationTypes.deleteCatalogSuccess)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.deleteCatalogFailed)
        })
    })
  },

  [actionTypes.updateCatalog](context, credentials) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.updateCatalogStart)

      CatalogDataService.update(credentials)
        .then((response) => {
          context.commit(mutationTypes.updateCatalogSuccess)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.updateCatalogFailed)
        })
    })
  },

  [actionTypes.createCatalog](context, credentials) {
    context.commit(mutationTypes.createCatalogStart)

    return new Promise((resolve) => {
      CatalogDataService.create(credentials)
        .then((response) => {
          context.commit(mutationTypes.createCatalogSuccess, response.data)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.createCatalogFailed)
        })
    })
  },

  [actionTypes.getComposition](context, {credentials, params}) {
    context.commit(mutationTypes.getCompositionStart)

    return new Promise((resolve) => {
      CatalogDataService.composition(credentials, params)
        .then((response) => {
          context.commit(mutationTypes.getCompositionSuccess, response.data)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.createCatalogFailed)
        })
    })
  },

  [actionTypes.addComposition](context, credentials) {
    context.commit(mutationTypes.addCompositionStart)

    return new Promise((resolve) => {
      CatalogDataService.addComposition(credentials)
        .then((response) => {
          context.commit(mutationTypes.addCompositionSuccess, response.data)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.addCompositionFailed)
        })
    })
  },

  [actionTypes.updateComposition](context, credentials) {
    context.commit(mutationTypes.updateCompositionStart)

    return new Promise((resolve) => {
      CatalogDataService.updateComposition(credentials)
        .then((response) => {
          context.commit(mutationTypes.updateCatalogSuccess, response.data)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.updateCatalogFailed)
        })
    })
  },

  [actionTypes.deleteComposition](context, credentials) {
    context.commit(mutationTypes.deleteCompositionStart)

    return new Promise((resolve) => {
      CatalogDataService.deleteComposition(credentials)
        .then((response) => {
          context.commit(mutationTypes.deleteCatalogSuccess)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.deleteCatalogFailed)
        })
    })
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}
