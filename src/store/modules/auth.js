import authService from '@/api/auth'
import {removeItem, setItem} from '@/helpers/persistanceStorage'

const state = {
  isLoading: false,
  profile: null,
  isLoggedIn: null,
}

export const mutationTypes = {
  signUpStart: '[auth] signUpStart',
  signUpSuccess: '[auth] signUpSuccess',
  signUpFailure: '[auth] signUpFailure',

  signInStart: '[auth] signInStart',
  signInSuccess: '[auth] signInSuccess',
  signInFailure: '[auth] signInFailure',

  getProfileStart: '[auth] getProfileStart',
  getProfileSuccess: '[auth] getProfileSuccess',
  getProfileFailed: '[auth] getProfileFailed',

  updateCurrentUserStart: '[auth] updateCurrentUserStart',
  updateCurrentUserSuccess: '[auth] updateCurrentUserSuccess',
  updateCurrentUserFailed: '[auth] updateCurrentUserFailed',

  signOutStart: '[auth] signOutStart',
  signOutSuccess: '[auth] signOutSuccess',
  signOutFailed: '[auth] signOutFailed',
}

export const actionTypes = {
  signUp: '[auth] signUp',
  signIn: '[auth] signIn',
  googleAuth: '[auth] googleAuth',
  googleCallback: '[auth] googleCallback',
  getProfile: '[auth] getProfile',
  updateCurrentUser: '[auth] updateCurrentUser',
  signOut: '[auth] signOut',
}

export const getterTypes = {
  profile: '[auth] profile',
  isLoggedIn: '[auth] isLoggedIn',
  isAnonymous: '[auth] isAnonymous',
  isAdmin: '[auth] isAdmin',
}

const getters = {
  [getterTypes.profile]: (state) => {
    return state.profile
  },
  [getterTypes.isLoggedIn]: (state) => {
    return Boolean(state.isLoggedIn)
  },
  [getterTypes.isAnonymous]: (state) => {
    return state.isLoggedIn === false
  },
  [getterTypes.isAdmin]: (state) => {
    if (
      'roles' in state.profile &&
      state.profile.roles.includes('admin')
    ) {
      return true
    }

    return false
  },
}

const mutations = {
  // SignUp
  [mutationTypes.signUpStart](state) {
    state.isLoading = true
  },
  [mutationTypes.signUpSuccess](state, payload) {
    state.profile = payload
    state.isLoggedIn = true
    state.isLoading = false
  },
  [mutationTypes.signUpFailure](state) {
    state.isLoading = false
  },

  // SignIn
  [mutationTypes.signInStart](state) {
    state.isLoading = true
  },
  [mutationTypes.signInSuccess](state) {
    state.isLoading = false
    state.isLoggedIn = true
  },
  [mutationTypes.signInFailure](state) {
    state.isLoading = false
  },

  // signOut
  [mutationTypes.signOutStart]() {},
  [mutationTypes.signOutSuccess](state) {
    state.isLoggedIn = false
    state.profile = null
  },
  [mutationTypes.signOutFailed]() {
    state.isLoggedIn = false
    state.profile = null
  },

  // getCurrentUser
  [mutationTypes.getProfileStart](state) {
    state.isLoading = true
  },
  [mutationTypes.getProfileSuccess](state, payload) {
    state.isLoading = false
    state.profile = payload
    state.isLoggedIn = true
  },
  [mutationTypes.getProfileFailed](state) {
    state.isLoading = false
    state.isLoggedIn = false
    state.profile = null
  },

  // updateCurrentUser
  [mutationTypes.updateCurrentUserStart]() {},
  [mutationTypes.updateCurrentUserSuccess](state, payload) {
    state.profile = payload
  },
  [mutationTypes.updateCurrentUserStart]() {},
}

const actions = {
  [actionTypes.signUp](context, credentials) {
    return new Promise((resolve, reject) => {
      context.commit(mutationTypes.signInStart)

      authService
        .signUp(credentials)
        .then((user) => {
          context.commit(mutationTypes.signUpSuccess, user)
          resolve(user)
        })
        .catch((result) => {
          context.commit(mutationTypes.signUpFailure, result.response.data)
          reject(result.response.data)
        })
    })
  },

  [actionTypes.signIn](context, credentials) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.signInStart)

      authService
        .signIn(credentials)
        .then((response) => {
          let tokens = response.data

          context.commit(mutationTypes.signInSuccess, tokens)
          setItem('accessToken', tokens.access_token)
          setItem('refreshToken', tokens.refresh_token)
          resolve(tokens)
        })
        .catch((result) => {
          context.commit(mutationTypes.signInFailure, result)
        })
    })
  },

  [actionTypes.googleAuth]() {
    return new Promise((resolve) => {
      authService
        .googleAuth()
        .then((url) => {
          resolve(url)
        })
        .catch(() => {})
    })
  },

  [actionTypes.googleCallback](context, credentials) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.signInStart)

      authService
        .googleCallback(credentials)
        .then((tokens) => {
          context.commit(mutationTypes.signInSuccess, tokens)
          setItem('accessToken', tokens.access_token)
          setItem('refreshToken', tokens.refresh_token)
          resolve(tokens)
        })
        .catch((result) => {
          context.commit(mutationTypes.signInFailure, result)
        })
    })
  },

  [actionTypes.getProfile](context) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.getProfileStart)

      authService
        .getProfile()
        .then((response) => {
          context.commit(mutationTypes.getProfileSuccess, response.data.data)
          resolve(response.data)
        })
        .catch(() => {
          context.commit(mutationTypes.getProfileFailed)
        })
    })
  },

  // [actionTypes.updateCurrentUser](context, {currentUserInput}) {
  //   return new Promise((resolve) => {
  //     context.commit(mutationTypes.updateCurrentUserStart)
  //
  //     authService
  //       .updateCurrentUser(currentUserInput)
  //       .then((user) => {
  //         context.commit(mutationTypes.updateCurrentUserSuccess, user)
  //         resolve(user)
  //       })
  //       .catch((result) => {
  //         context.commit(
  //           mutationTypes.updateCurrentUserFailed,
  //           result.response.data.errors
  //         )
  //       })
  //   })
  // },
  [actionTypes.signOut](context) {
    return new Promise((resolve) => {
      context.commit(mutationTypes.signOutStart)

      authService
        .signOut()
        .then(() => {
          context.commit(mutationTypes.signOutSuccess)
          removeItem('accessToken')
          removeItem('refreshToken')
          resolve()
        })
        .catch((result) => {
          context.commit(
            mutationTypes.signOutFailed,
            result.response.data.errors
          )

          removeItem('accessToken')
          removeItem('refreshToken')
        })
    })
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}
