const state = {
  breadcrumbs: []
}

export const mutationTypes = {
  setBreadCrumbs: '[bread] setBreadCrumbs'
}

export const actionTypes = {
  setBreadCrumbs: '[bread] setBreadCrumbs'
}

export const getterTypes = {
  breadcrumbs: '[bread] breadcrumbs'
}

const getters = {
  [getterTypes.breadcrumbs]: state => {
    return state.breadcrumbs
  }
}

const mutations = {
  [mutationTypes.setBreadCrumbs](state, payload) {
    state.breadcrumbs = payload
  }
}

const actions = {
  [actionTypes.setBreadCrumbs](context, items) {
    return new Promise(resolve => {
      context.commit(mutationTypes.setBreadCrumbs, items)
      resolve(items)
    })
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
