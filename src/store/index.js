import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/modules/auth'
import bread from '@/store/modules/bread'
import catalog from '@/store/modules/catalog'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// const ls = new SecureLS({isCompression: false})

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    bread,
    catalog,
  },
})
