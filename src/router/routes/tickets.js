export default [
  {
    path: '/tickets',
    name: 'Tickets',
    component: () => import('@/views/Ticket/Tickets'),
    meta: {
      auth: true,
    },
  },
]
