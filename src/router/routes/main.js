export default [
  {
    path: '',
    name: 'Home',
    component: () => import('@/views/Home')
  },
  {
    path: '/secure',
    name: 'Secure',
    component: () => import('@/views/Secure'),
    meta: {
      auth: true
    }
  },
  {
    path: '*',
    redirect: {name: 'Home'}
  }
]
