export default [
  // {
  //   path: '/auth/signup',
  //   name: 'Registration',
  //   component: () => import('@/views/Auth/Registration')
  // },
  {
    path: '/auth/signin',
    name: 'Login',
    component: () => import('@/views/Auth/Login'),
  },
  {
    path: '/callback/:slug',
    name: 'Callback',
    component: () => import('@/views/Auth/Callback'),
  },
]
