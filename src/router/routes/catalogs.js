export default [
  {
    path: '/catalogs',
    name: 'Catalogs',
    component: () => import('@/views/Catalog/CatalogList'),
    meta: {
      auth: true,
    },
  },
  {
    path: '/catalogs/:id/composition',
    name: 'Composition',
    component: () => import('@/views/Catalog/Composition'),
  },
]
