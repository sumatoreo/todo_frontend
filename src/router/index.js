import Vue from 'vue'
import VueRouter from 'vue-router'
import mainRoutes from '@/router/routes/main'
import authRoutes from '@/router/routes/auth'
import ticketsRoutes from '@/router/routes/tickets'
import catalogsRoutes from '@/router/routes/catalogs'
import {getItem} from '@/helpers/persistanceStorage'

Vue.use(VueRouter)

const {isNavigationFailure, NavigationFailureType} = VueRouter
const routes = [
  ...mainRoutes,
  ...authRoutes,
  ...ticketsRoutes,
  ...catalogsRoutes,
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

const isAuthToken = () => {
  return getItem('accessToken')

  // if (authToken && authToken.length > 0) {
  //   return true
  // let payload = authToken.split('.')[1]
  // payload = JSON.parse(atob(payload))
  //
  // return Date.now() < payload.exp * 1000
  // }

  // return false
}

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.auth) {
    if (isAuthToken()) {
      next()
    } else {
      next({name: 'Login'})
    }

    return
  }

  if (isAuthToken()) {
    if (['Login', 'Registration'].includes(to.name)) {
      if (from.name !== 'Tickets') {
        next({name: 'Tickets'})
      }

      return
    }
  }

  next()
})

// https://stackoverflow.com/questions/57837758/navigationduplicated-navigating-to-current-location-search-is-not-allowed
function patchRouterMethod(router, methodName) {
  router['_' + methodName] = router[methodName]
  router[methodName] = async function (location) {
    return router['_' + methodName](location).catch((error) => {
      if (error.name === 'NavigationDuplicated') {
        return this.currentRoute
      }

      if (isNavigationFailure(error, NavigationFailureType.redirected)) {
        return this.currentRoute
      }

      throw error
    })
  }
}

patchRouterMethod(router, 'push')
patchRouterMethod(router, 'replace')

export default router
