module.exports = {
  transpileDependencies: ['vuetify'],
  devServer: {
    // open: true,
    open: process.platform === 'darwin',
    host: 'localhost',
    port: 3000,
    https: true,
    hotOnly: false,
  },
  chainWebpack: (config) => {
    config.plugin('html').tap((args) => {
      args[0].title = 'ToDo List'
      return args
    })
  },
}
